package net.zoranpavlovic.data.spring.service;

import net.zoranpavlovic.data.spring.entity.NoteJpa;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
public interface NotesService {

    NoteJpa addNote(NoteJpa note);

    List<NoteJpa> getAll();
}
