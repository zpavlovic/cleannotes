package net.zoranpavlovic.data.spring.mapper;

import net.zoranpavlovic.data.spring.entity.NoteJpa;
import net.zoranpavlovic.domain.model.Note;

public class NoteMapper {

    public static NoteMapper instance;

    public static NoteMapper getInstance(){
        if(instance == null){
            return instance = new NoteMapper();
        }
        return instance;
    }

    public Note convertToPlainModel(NoteJpa noteJpa){
        return new Note(noteJpa.getId(), noteJpa.getText(), noteJpa.getDate());
    }

    public NoteJpa convertToJpaModel(Note note){
        return new NoteJpa(note.getId(), note.getText(), note.getDate());
    }

}