package net.zoranpavlovic.data.spring;

import net.zoranpavlovic.data.spring.entity.NoteJpa;
import net.zoranpavlovic.data.spring.service.NotesService;
import net.zoranpavlovic.data.spring.mapper.NoteMapper;
import net.zoranpavlovic.domain.core.NotesDataSource;
import net.zoranpavlovic.domain.model.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
@Component
public class DatabaseDataSource implements NotesDataSource {

    @Autowired
    private NotesService notesService;

    public DatabaseDataSource(){
    }

    @Override
    public Note addNote(Note note) {
        NoteJpa noteJpa = notesService.addNote(NoteMapper.getInstance().convertToJpaModel(note));
        return NoteMapper.getInstance().convertToPlainModel(noteJpa);
    }

    @Override
    public List<Note> getAll() {
        List<NoteJpa> notesJpa = notesService.getAll();
        List<Note> notes = new ArrayList<>();
        for(NoteJpa noteJpa : notesJpa){
            notes.add(NoteMapper.getInstance().convertToPlainModel(noteJpa));
        }
        return notes;
    }
}
