package net.zoranpavlovic.data.spring.repository;

import net.zoranpavlovic.domain.core.NotesDataSource;
import net.zoranpavlovic.domain.core.NotesRepository;
import net.zoranpavlovic.domain.model.Note;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
public class NotesRepositoryImpl implements NotesRepository {

    private NotesDataSource dataSource;

    public NotesRepositoryImpl(NotesDataSource dataSource){
        this.dataSource = dataSource;
    }

    @Override
    public Note addNote(Note note) {
        return dataSource.addNote(note);
    }

    @Override
    public List<Note> getAll() {
        return dataSource.getAll();
    }
}
