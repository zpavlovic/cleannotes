package net.zoranpavlovic.domain.add;

import net.zoranpavlovic.domain.model.Note;

/**
 * Created by Zoran on 20/04/2017.
 */
public interface AddNoteInteractor {

    Note addNote(Note note);
}
