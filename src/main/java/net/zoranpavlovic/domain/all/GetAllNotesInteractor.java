package net.zoranpavlovic.domain.all;

import net.zoranpavlovic.domain.model.Note;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
public interface GetAllNotesInteractor {

    List<Note> getAll();

}
