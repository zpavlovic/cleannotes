package net.zoranpavlovic.domain.all;

import net.zoranpavlovic.domain.model.Note;
import net.zoranpavlovic.domain.core.NotesRepository;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
public class GetAllNotesInteractorImpl implements GetAllNotesInteractor {

    private NotesRepository repository;

    public GetAllNotesInteractorImpl(NotesRepository repository){
        this.repository = repository;
    }

    @Override
    public List<Note> getAll() {
        return repository.getAll();
    }
}
