package net.zoranpavlovic.domain.core;

import net.zoranpavlovic.domain.model.Note;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
public interface NotesRepository {

    Note addNote(Note note);

    List<Note> getAll();
}
