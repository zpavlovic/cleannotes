package net.zoranpavlovic.presentation.rest.presenter;

import net.zoranpavlovic.data.spring.DatabaseDataSource;
import net.zoranpavlovic.data.spring.repository.NotesRepositoryImpl;
import net.zoranpavlovic.data.spring.service.NotesService;
import net.zoranpavlovic.domain.all.GetAllNotesInteractorImpl;
import net.zoranpavlovic.domain.core.NotesDataSource;
import net.zoranpavlovic.domain.core.NotesRepository;
import net.zoranpavlovic.domain.model.Note;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
public class GetAllNotesPresenterImpl implements GetAllNotesPresenter {

    private NotesRepository repository;
    private GetAllNotesInteractorImpl interactor;

    public GetAllNotesPresenterImpl(NotesDataSource dataSource){

        repository = new NotesRepositoryImpl(dataSource);
        interactor = new GetAllNotesInteractorImpl(repository);
    }


    @Override
    public List<Note> getAll() {
        return repository.getAll();
    }
}
