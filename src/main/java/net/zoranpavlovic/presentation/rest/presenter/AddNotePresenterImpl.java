package net.zoranpavlovic.presentation.rest.presenter;

import net.zoranpavlovic.data.spring.DatabaseDataSource;
import net.zoranpavlovic.data.spring.repository.NotesRepositoryImpl;
import net.zoranpavlovic.data.spring.service.NotesService;
import net.zoranpavlovic.domain.add.AddNoteInteractor;
import net.zoranpavlovic.domain.add.AddNoteInteractorImpl;
import net.zoranpavlovic.domain.core.NotesDataSource;
import net.zoranpavlovic.domain.core.NotesRepository;
import net.zoranpavlovic.domain.model.Note;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Zoran on 20/04/2017.
 */
public class AddNotePresenterImpl implements AddNotePresenter {

    private NotesRepository repository;
    private AddNoteInteractor interactor;

    public AddNotePresenterImpl(NotesDataSource dataSource){
        repository = new NotesRepositoryImpl(dataSource);
        interactor = new AddNoteInteractorImpl(repository);
    }

    @Override
    public Note addNote(Note note) {
        return repository.addNote(note);
    }
}
