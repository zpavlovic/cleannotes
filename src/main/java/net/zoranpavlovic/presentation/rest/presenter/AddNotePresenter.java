package net.zoranpavlovic.presentation.rest.presenter;

import net.zoranpavlovic.domain.model.Note;

/**
 * Created by Zoran on 20/04/2017.
 */
public interface AddNotePresenter {
     Note addNote(Note note);
}
