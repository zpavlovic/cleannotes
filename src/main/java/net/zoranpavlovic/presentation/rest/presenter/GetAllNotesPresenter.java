package net.zoranpavlovic.presentation.rest.presenter;

import net.zoranpavlovic.domain.model.Note;

import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */
public interface GetAllNotesPresenter {

    List<Note> getAll();
}
