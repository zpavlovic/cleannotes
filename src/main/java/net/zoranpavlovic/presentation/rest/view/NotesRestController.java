package net.zoranpavlovic.presentation.rest.view;

import net.zoranpavlovic.data.spring.service.NotesService;
import net.zoranpavlovic.domain.core.NotesDataSource;
import net.zoranpavlovic.domain.model.Note;
import net.zoranpavlovic.presentation.rest.presenter.AddNotePresenter;
import net.zoranpavlovic.presentation.rest.presenter.AddNotePresenterImpl;
import net.zoranpavlovic.presentation.rest.presenter.GetAllNotesPresenter;
import net.zoranpavlovic.presentation.rest.presenter.GetAllNotesPresenterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.util.List;

/**
 * Created by Zoran on 20/04/2017.
 */

@RestController
@RequestMapping(value="notes/api")
public class NotesRestController {

    private GetAllNotesPresenter getAllNotesPresenter;
    private AddNotePresenter addNotePresenter;

    @Autowired
    private NotesDataSource dataSource;

    @RequestMapping(value="/all")
    public List<Note> getAll(){
        getAllNotesPresenter = new GetAllNotesPresenterImpl(dataSource);
        return getAllNotesPresenter.getAll();
    }

    @RequestMapping(value = "/add")
    public Note addNote(@RequestParam(value="id") int id, @RequestParam(value="text") String text, @RequestParam(value="date") Date date){
        Note note = new Note(id, text, date);
        addNotePresenter = new AddNotePresenterImpl(dataSource);
        return  addNotePresenter.addNote(note);
    }
}
