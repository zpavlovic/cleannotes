package net.zoranpavlovic.presentation.web;

import net.zoranpavlovic.domain.core.NotesDataSource;
import net.zoranpavlovic.domain.model.Note;
import net.zoranpavlovic.presentation.rest.presenter.AddNotePresenter;
import net.zoranpavlovic.presentation.rest.presenter.GetAllNotesPresenter;
import net.zoranpavlovic.presentation.rest.presenter.GetAllNotesPresenterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by Zoran on 21/04/2017.
 */
@Controller
public class NotesController {

    private GetAllNotesPresenter getAllNotesPresenter;

    @Autowired
    private NotesDataSource dataSource;

    @RequestMapping(value = "/")
    public String index(Model model){
        model.addAttribute("notes", getAllNotes());
        return "index";
    }

    private List<Note> getAllNotes() {
        getAllNotesPresenter = new GetAllNotesPresenterImpl(dataSource);
        return getAllNotesPresenter.getAll();
    }

}
